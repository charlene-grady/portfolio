def find_target_in_matrix(matrix, target):
    # Check if the matrix is empty
    if not matrix or not matrix[0]:
        print("Target not in matrix")
        return

    rows = len(matrix)
    columns = len(matrix[0])

    # Start from the top-right corner of the matrix
    row = 0
    col = columns - 1

    # Search for the target in the matrix
    while row < rows and col >= 0:
        if matrix[row][col] == target:
            print(f"Target {target} found at row {row}, column {col}")
            return
        elif matrix[row][col] > target:
            col -= 1  # Move left
        else:
            row += 1  # Move down

    print(f"Target {target} not in matrix")

# Example matrix
matrix = [
    [1, 4, 7, 12, 15, 1000],
    [2, 5, 19, 31, 32, 1001],
    [3, 8, 24, 33, 35, 1002],
    [40, 41, 42, 44, 45, 1003],
    [99, 100, 103, 106, 128, 1004]
]

# Print the matrix
print("Matrix:")
for row in matrix:
    print(row)

# Input target from user
target = int(input("Enter the target value: "))

# Call the function
find_target_in_matrix(matrix, target)
