def factorial(n, lookup_table={0: 1}):
    # Check if factorial is already in the table
    if n in lookup_table:
        return lookup_table[n]

    # Calculate factorial iteratively if not found
    result = 1
    for i in range(1, n + 1):
        result *= i
        lookup_table[i] = result  # Store result in table for future lookups
    return result

# Enter your number
number = int(input("Enter a number: "))
fact = factorial(number)
print(f"The factorial of {number} is: {fact}")