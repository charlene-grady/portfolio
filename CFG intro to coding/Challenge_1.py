def round_grade(grade):
    if grade < 40:
        return grade  # No rounding if the grade is less than 40
    next_multiple_of_5 = (grade + 4) // 5 * 5  # Calculate the next multiple of 5
    if next_multiple_of_5 - grade < 3:
        return next_multiple_of_5  # Round up to the next multiple of 5 if the difference is less than 3
    else:
        return grade  # No rounding needed

def determine_grade_status(grade):
    rounded_grade = round_grade(grade)
    if rounded_grade < 40:
        return f"Grade: {rounded_grade}. Status: Failed"
    elif rounded_grade > 80:
        return f"Grade: {rounded_grade}. Status: Distinction"
    else:
        return f"Grade: {rounded_grade}. Status: Passed"

# Example usage
input_grade = int(input("Enter the grade: "))
print(determine_grade_status(input_grade))