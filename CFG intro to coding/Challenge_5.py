import random

def generate_random_credit_card_number():
    card_number = ''
    for i in range(16):
        card_number += str(random.randint(0, 9))
        if (i + 1) % 4 == 0 and i != 15:
            card_number += ' '
    return card_number

def hide_credit_card_number(card_number):
    return 'XXXX XXXX XXXX ' + card_number[-4:]

random_card_number = generate_random_credit_card_number()
hidden_card_number = hide_credit_card_number(random_card_number)

print("Randomly generated credit card number:", random_card_number)
print("After hiding the first 12 digits:", hidden_card_number)