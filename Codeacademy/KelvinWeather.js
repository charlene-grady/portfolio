/* This is kelvin forcast today */
const kelvin = 293;
/* The difference between celsius and the forcast */
const celsius = kelvin - 273;
/* Converting from celsius to fahrenheit */
/* Using the .floor method to round down */
let fahrenheit = celsius * (9/5) + 32;
fahrenheit = Math.floor(fahrenheit);

console.log(fahrenheit)